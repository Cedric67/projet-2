#ifndef UPDRINKS_H
#define UPDRINKS_H

/*!
 * \file updrinks.h
 * \brief Description de la classe UpDrinks.
 * \details La classe \b UpDrinks permet d'envoyer les informations à modifier à la classe \b database de part sa fenêtre.
 */

#include <QDialog>
#include "database.h"
#include "drinks.h"

/*!
 * \namespace Ui
 * \brief Déclaration d'utilisation de la \b Forms updrinks.ui.
 */
namespace Ui {

/*!
 * \class UpDrinks
 * \brief Intégration de la classe UpDrinks dans le \b namespace.
 */
class UpDrinks;
}

/*!
 * \class UpDrinks
 * \brief La classe UpDrinks hérite de l'objet QDialog, elle contient tous les attribues et fonctions nécessaires à la mise à jour d'une boisson.
 */
class UpDrinks : public QDialog
{
    Q_OBJECT

public:
    explicit UpDrinks(QWidget *parent = 0);
    ~UpDrinks();

    /*!
     * \brief Pointeur vers l'IHM UpDrinks.ui.
     */
    Ui::UpDrinks *ui;

    /*!
     * \brief Booléen indiquant si une modification a été apporté à la boisson.
     */
    bool change;

    /*!
     * \brief Id de la boisson sélectionnée.
     */
    int id;

private slots:
    /*!
     * \fn on_buttonBox_accepted
     * \brief Fonction qui valide les modifications apportés à la boisson.
     */
    void on_buttonBox_accepted();

    /*!
     * \fn on_buttonBox_rejected
     * \brief Fonction qui annule les modifications.
     */
    void on_buttonBox_rejected();

private:
    /*!
     * \brief Objet qui permet l'utillisation de la base de données.
     */
    database bdd;

    /*!
     * \brief Pointeur de l'objet drinks.
     */
    drinks* drink = new drinks;
};

#endif // UPDRINKS_H
