#ifndef MONEY_H
#define MONEY_H

/*!
 * \file money.h
 * \brief Description de la classe Money.
 * \details La classe \b Money permet l'injection virtuelle de monnaie dans le distributeur de part sa fenêtre.
 */

#include <QDialog>
#include <QCloseEvent>

/*!
 * \namespace Ui
 * \brief Déclaration d'utilisation de la \b Forms money.ui.
 */
namespace Ui {

/*!
 * \class Money
 * \brief Intégration de la classe Money dans le \b namespace.
 */
class Money;
}

/*!
 * \class Money
 * \brief La classe Money hérite de l'objet QDialog, elle contient tous les attribues et fonctions nécessaires à la simulation d'injection de monnaie.
 */
class Money : public QDialog
{
    Q_OBJECT

public:
    explicit Money(QWidget *parent = 0);
    ~Money();

    /*!
     * \brief Pointeur vers l'IHM money.ui.
     */
    Ui::Money *ui;

    /*!
     * \fn void closeEvent
     * \param *e
     * \brief Fonction surcharge de la méthode closeEvent.
     * \details Contrôle la fermeture de la fenêtre et empêche la fermeture par la croix.
     */
    void closeEvent(QCloseEvent *e);

    /*!
     * \brief Float qui réccupère la somme de la monnaie injectée.
     */
    float amount = 0;

    /*!
     * \brief Booléen qui autorise ou non la fermeture de la fenêtre.
     */
    bool cancel;


public slots:
    /*!
     * \fn void on_pushButton_clicked
     * \brief Fonction qui lance la fonction \b sum avec la valeur \b 0.10.
     */
    void on_pushButton_clicked();

    /*!
     * \fn void on_pushButton_2_clicked
     * \brief Fonction qui lance la fonction \b sum avec la valeur \b 0.20.
     */
    void on_pushButton_2_clicked();

    /*!
     * \fn void on_pushButton_3_clicked
     * \brief Fonction qui lance la fonction \b sum avec la valeur \b 0.50.
     */
    void on_pushButton_3_clicked();

    /*!
     * \fn void on_pushButton_4_clicked
     * \brief Fonction qui lance la fonction \b sum avec la valeur \b 1.
     */
    void on_pushButton_4_clicked();

    /*!
     * \fn void on_pushButton_5_clicked
     * \brief Fonction qui lance la fonction \b sum avec la valeur \b 2.
     */
    void on_pushButton_5_clicked();

private:
    /*!
     * \fn void sum
     * \param value
     * \brief Calcul la somme de la monnaie injecté et l'affecte à la varible \b amount.
     */
    void sum(float value);
};

#endif // MONEY_H
