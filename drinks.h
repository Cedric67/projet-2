#ifndef DRINKS_H
#define DRINKS_H

/*!
 * \file drinks.h
 * \brief Description de la classe drinks.
 * \details La classe \b drinks contient les attribues des boissons.
 */

#include <QString>

/*!
 * \class drinks
 * \brief La classe drinks contient 4 attribues nécessaires pour les boissons.
 */
class drinks
{
public:
    /*!
     * \brief drinks
     */
    drinks();

    /*!
     * \brief Integer prenant l'\b id de la boisson.
     */
    int id;

    /*!
     * \brief String prenant le \b nom de la boisson.
     */
    QString name;

    /*!
     * \brief Float prenant le \b prix de la boisson.
     */
    float price;

    /*!
     * \brief Integer prenant la \b quantité \b vendu de la boisson.
     */
    int qtySold;
};

#endif // DRINKS_H
