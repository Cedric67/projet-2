#include "database.h"

database::database()
{

}

void database::addDatabase(QString name)
{
    this->name = name;
    QDir dir;
    db = QSqlDatabase::addDatabase("QSQLITE");
    QString DBPath = dir.currentPath() + "/Database/";
    if (!dir.exists(DBPath))
    {
        dir.mkdir(DBPath);
        qDebug() << "Création du répertoire contenant la BDD";
    }
    db.setDatabaseName(DBPath + name + ".db");
    qDebug() << "Création de la BDD : " + DBPath;
}

void database::initializeDatabase(QString name)
{
    if (name == "users")
    {
        if (countRows(name) == 0)
        {
            qDebug() << "Ajout de l'utilisateur administrateur dans la table users";
            addUser();
        }
    }else if (name == "drinks"){
        if (countRows(name) == 0)
        {
            qDebug() << "Ajout des boissons dans la table drinks";
            addDrinks();
        }
    }
}

bool database::openDatabase()
{
    if (!db.open()) {
        qDebug() << "Echec de l'ouvert de la BDD " << this->name;
        return false;
    }else{
        qDebug() << "Ouverture de la BDD " << this->name << " reussi";
        return true;
    }
}

void database::closeDatabase()
{
    qDebug() << "Fermeture de la BDD " << this->name;
    db.close();
}

bool database::executeQuery(QString request)
{
    QSqlQuery query(this->db);
    if (!query.exec(request))
    {
        qDebug() << "Echec de l'execution de la requete : " << request;
        return false;
    }else{
        qDebug() << "Execution reussite de la requete : " << request;
        return true;
    }
}

int database::addUser()
{
    int nbUsers = countRows("users");
    int newId = 0;
    if (nbUsers < 10000)
    {
        QString request;
        QVector<int> user;

        if (nbUsers > 0)
        {
            request = "SELECT id FROM users ORDER BY id";
            QSqlQuery query(this->db);
            if (query.exec(request))
            {
                for (int i = 0; i < nbUsers; ++i)
                {
                    query.next();
                    user.append(query.value(0).toInt());
                }
            }

            for (int i = 0; i < user.length(); ++i)
            {
                if (user.value(i) + 1 != user.value(i+1))
                {
                    newId = i + 1;
                    break;
                }
            }
        }else{
            newId = user.count();
        }

        request = "INSERT INTO users VALUES(" + QString::number(newId) + ",0)";
        executeQuery(request);
    }else{
        newId = -1;
    }

    qDebug() << "Ajout de l'utilisateur avec l'id : " << newId;
    return newId;
}

bool database::deleteUser(int id)
{
    QSqlQuery query(this->db);
    query.prepare("DELETE FROM users WHERE id=?");
    query.addBindValue(id);

    if (!query.exec())
    {
        qDebug() << "Echec de la suppression de l'utilisateur possedant l'id : " << id;
        return false;
    }else{
        qDebug() << "Reussite de la suppression de l'utilisateur possedant l'id : " << id;
        return true;
    }
}

bool database::selectUser(users* user, int id)
{
    QString request = "SELECT EXISTS(SELECT * FROM users WHERE id=" + QString::number(id) + ")";
    QSqlQuery query;
    query.exec(request);
    query.next();
    if (query.value(0).toInt() == 0)
    {
        qDebug() << "Echec : L'utilisateur " << id << " n'existe pas";
        return false;
    }else{
        request = "SELECT * FROM users WHERE id=" + QString::number(id);
        query.exec(request);
        query.next();

        user->id = query.value(0).toInt();
        user->credit = query.value(1).toFloat();

        qDebug() << "L'utilisateur " << id << " a " << user->credit << "E en credit";
        return true;
    }

}

bool database::updateCredit(users* user)
{
    QSqlQuery query(this->db);
    query.prepare("UPDATE users SET credits=? WHERE id=?");
    query.addBindValue(user->credit);
    query.addBindValue(user->id);

    if (!query.exec())
    {
        qDebug() << "Echec de la requete de la mise a jour du credit de l'utilisateur";
        return false;
    }else{
        qDebug() << "Execution de la requete de la mise a jour du credit de l'utilisateur reussi";
        return true;
    }
}

void database::addDrinks()
{
    qDebug() << "Ajout des boissons dans la table drinks";
    QString drinksNameTab[12];
    float drinksPriceTab[12];

    drinksNameTab[0] = "Cristaline";
    drinksPriceTab[0] = 0.80;
    drinksNameTab[1] = "Lipton Ice Tea";
    drinksPriceTab[1] = 1.00;
    drinksNameTab[2] = "Coca Cola";
    drinksPriceTab[2] = 1.50;
    drinksNameTab[3] = "Coca Cola light";
    drinksPriceTab[3] = 1.50;
    drinksNameTab[4] = "Coca Cola zero";
    drinksPriceTab[4] = 1.50;
    drinksNameTab[5] = "Orangina";
    drinksPriceTab[5] = 1.80;
    drinksNameTab[6] = "Sprite";
    drinksPriceTab[6] = 1.40;
    drinksNameTab[7] = "7 Up";
    drinksPriceTab[7] = 1.00;
    drinksNameTab[8] = "Schweppes Tonic";
    drinksPriceTab[8] = 1.20;
    drinksNameTab[9] = "Canada Dry";
    drinksPriceTab[9] = 2.00;
    drinksNameTab[10] = "Gini";
    drinksPriceTab[10] = 1.80;
    drinksNameTab[11] = "Red Bull";
    drinksPriceTab[11] = 1.20;

    for (int i = 0; i<12; i++)
    {
        QString request = "INSERT INTO drinks VALUES(" + QString::number(i) + ",'" + drinksNameTab[i] + "',";
        request += QString::number(drinksPriceTab[i]) + ",0)";
        executeQuery(request);
    }
}

bool database::updateDrink(drinks* drink)
{
    QSqlQuery query(this->db);
    query.prepare("UPDATE drinks SET nom=?, prix=?, qtevendue=? WHERE id=?");
    query.addBindValue(drink->name);
    query.addBindValue(drink->price);
    query.addBindValue(drink->qtySold);
    query.addBindValue(drink->id);
    //qDebug() << "Request" << request;

    if (!query.exec())
    {
        qDebug() << "Echec de la mise à jour de la boisson";
        return false;
    }else{
        qDebug() << "Mise à jour de la boisson reussite";
        return true;
    }
}

void database::selectDrink(drinks* drink, int id)
{
    QString request = "SELECT * FROM drinks WHERE id=" + QString::number(id) + ";";
    QSqlQuery query(this->db);
    query.exec(request);
    if (query.next())
    {
        drink->id = query.value(0).toInt();
        drink->name = query.value(1).toString();
        drink->price = query.value(2).toFloat();
        drink->qtySold = query.value(3).toInt();
    }
    qDebug() << "Selection de la boisson " << id;
}

int database::countRows(QString name)
{
    QString request = "SELECT COUNT(*) FROM " + name;
    QSqlQuery query(this->db);
    query.exec(request);
    query.next();

    qDebug() << "La table " << name << " a " << query.value(0).toInt() << "lignes";
    return query.value(0).toInt();
}
