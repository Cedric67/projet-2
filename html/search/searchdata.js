var indexSectionsWithContent =
{
  0: "abcdefimnopqstuw",
  1: "dmu",
  2: "u",
  3: "dmu",
  4: "abcdeimopsuw",
  5: "abcdfimnpqstu"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables"
};

var indexSectionLabels =
{
  0: "Tout",
  1: "Classes",
  2: "Espaces de nommage",
  3: "Fichiers",
  4: "Fonctions",
  5: "Variables"
};

