#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    Initialize();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::Initialize()
{
    setWindowIcon(QIcon(":pictures/pictures/icon.ico"));

    ui->label_2->setPixmap(QPixmap(":/pictures/pictures/CRISTALINE.png"));
    ui->label_3->setPixmap(QPixmap(":/pictures/pictures/Lipton-ice-tea.png"));
    ui->label_4->setPixmap(QPixmap(":/pictures/pictures/Coca-cola.png"));
    ui->label_5->setPixmap(QPixmap(":/pictures/pictures/Coca-cola-light.png"));
    ui->label_6->setPixmap(QPixmap(":/pictures/pictures/Coca-cola-zero.png"));
    ui->label_7->setPixmap(QPixmap(":/pictures/pictures/Orangina.png"));
    ui->label_8->setPixmap(QPixmap(":/pictures/pictures/Sprite.png"));
    ui->label_9->setPixmap(QPixmap(":/pictures/pictures/7-up.png"));
    ui->label_10->setPixmap(QPixmap(":/pictures/pictures/Schweppes-tonic.png"));
    ui->label_11->setPixmap(QPixmap(":/pictures/pictures/Canada-Dry.png"));
    ui->label_12->setPixmap(QPixmap(":/pictures/pictures/Gini.png"));
    ui->label_13->setPixmap(QPixmap(":/pictures/pictures/RedBull.png"));

    ui->lInfo->setText("Pour sélectionner une boisson, veuillez\ncomposer son numéro à 2 chiffres puis\ncliquez sur le bouton *.\nPour annuler cliquez sur le bouton #.");

    this->fMessage = "Bienvenue !";
    this->sMessage = "Démarrage en cours...";
    setLabel(this->fMessage, this->sMessage);
    QTimer::singleShot(5000, this, SLOT(welcomeMessage()));

    this->bdd.addDatabase("database");
    this->bdd.openDatabase();
    this->bdd.executeQuery("CREATE TABLE IF NOT EXISTS users (id INT, credits FLOAT)");
    this->bdd.initializeDatabase("users");
    this->bdd.executeQuery("CREATE TABLE IF NOT EXISTS drinks (id INT, nom TEXT, prix FLOAT, qtevendue INT)");
    this->bdd.initializeDatabase("drinks");
}

void MainWindow::closeEvent(QCloseEvent *e)
{
    if (this->cancel == false) {
        e->ignore();
    }else{
        e->accept();
    }
}

void MainWindow::welcomeMessage()
{
    this->screen = 0;
    this->memory = "";
    this->fMessage = "Veuillez sélectionner une boisson";
    this->sMessage = "";
    setLabel(this->fMessage);
}

void MainWindow::setLabel(QString Line)
{
    ui->lScreen->setText(Line + "\n");
}

void MainWindow::setLabel(QString fLine, QString sLine)
{
    ui->lScreen->setText(fLine + "\n" + sLine);
}

void MainWindow::adminMode()
{
    this->screen = 5;
    this->fMessage = "Sélectionnez l'action";
    adminMessage();
}

void MainWindow::adminMessage()
{
    if (this->memory == "")
    {
        connect(t, SIGNAL(timeout()), this, SLOT(adminMessage2()));
        t->start(2000);
        this->sMessage = "1.Ajouter utilisateur 2.Supprimer utilisateur";
        setLabel(this->fMessage, this->sMessage);
    }else{
        t->stop();
    }
}

void MainWindow::adminMessage2()
{
    if (this->memory == "")
    {
        connect(t, SIGNAL(timeout()), this, SLOT(adminMessage3()));
        t->start(2000);
        this->sMessage = "3.Modifier Boisson 4.Voir consommation d'une boisson";
        setLabel(this->fMessage, this->sMessage);
    }else{
        t->stop();
    }
}

void MainWindow::adminMessage3()
{
    if (this->memory == "")
    {
        connect(t, SIGNAL(timeout()), this, SLOT(adminMessage()));
        t->start(2000);
        this->sMessage = "5. Arrêter distributeur";
        setLabel(this->fMessage, this->sMessage);
    }else{
        t->stop();
    }
}

void MainWindow::pay(int userType)
{
    m.cancel = true;
    m.close();

    drink->qtySold += 1;
    bdd.updateDrink(drink);

    this->fMessage = "Merci à bientôt";

    float tmp;
    if ((drink->price - user->credit) < 0.1){
        tmp = 0;
    }else{
        tmp = user->credit - drink->price;
    }

    switch (userType) {
    case 0:
        this->sMessage = "Ancien solde : " + priceFormat(user->credit) + "€ Nouveau solde : " + priceFormat(tmp) + "€";

        user->credit -= drink->price;
        bdd.updateCredit(user);
        break;
    case 1:
        this->sMessage = "Payer : " + priceFormat(this->memory.toFloat()) + "€ Rendu : " + priceFormat(this->memory.toFloat() - drink->price) + "€";
        break;
    case 2:
        this->sMessage = "Ancien solde : " + priceFormat(user->credit) + "€ Nouveau solde : " + priceFormat(tmp + this->memory.toFloat()) + "€";

        user->credit = user->credit - drink->price + this->memory.toFloat();
        bdd.updateCredit(user);
        break;
    }
    setLabel(this->fMessage, this->sMessage);
    QTimer::singleShot(3000, this, SLOT(welcomeMessage()));
}

void MainWindow::showMoney()
{
    m.setWindowIcon(QIcon(":pictures/pictures/icon.ico"));
    m.showNormal();

    connect(m.ui->pushButton,SIGNAL(clicked()),SLOT(addMoney()));
    connect(m.ui->pushButton_2,SIGNAL(clicked()),SLOT(addMoney()));
    connect(m.ui->pushButton_3,SIGNAL(clicked()),SLOT(addMoney()));
    connect(m.ui->pushButton_4,SIGNAL(clicked()),SLOT(addMoney()));
    connect(m.ui->pushButton_5,SIGNAL(clicked()),SLOT(addMoney()));
}

void MainWindow::addMoney()
{
    this->memory = QString::number(m.amount);
    setLabel(this->fMessage, this->sMessage + priceFormat(this->memory.toFloat()) + "€");
}

void MainWindow::updateDrink()
{
    if (u.change == true)
    {
        bdd.selectDrink(drink, u.id);
        this->fMessage = "Vous avez changer la boisson portant l'Id : " + idFormatD(drink->id);
        this->sMessage = "Nom : " + drink->name + " Prix : " + priceFormat(drink->price) + "€";
        setLabel(this->fMessage, this->sMessage);
    }
    this->memory = "";
    connect(t, SIGNAL(timeout()), this, SLOT(adminMode()));
    t->start(4000);
}

void MainWindow::buttonAction(int buttonId)
{
    switch (this->screen)
    {
    case 0:
        selection(buttonId, 12);
        break;
    case 1:
        if ((buttonId == 1 || buttonId == 2) && this->memory == "")
        {
            this->memory = QString::number(buttonId);
            setLabel(this->fMessage, this->sMessage + this->memory);
        }
        break;
    case 2:
        selection(buttonId, 9999);
        break;
    case 5:
        if ((buttonId > 0 && buttonId < 6) && this->memory == "")
        {
            this->memory = QString::number(buttonId);
            setLabel(this->fMessage, this->memory);
        }
        break;
    case 6:
        selection(buttonId, 9999);
        break;
    case 7:
        selection(buttonId, 12);
        break;
    default:
        break;
    }
}

void MainWindow::selection(int value, int nbSize)
{
    QString tmp = QString::number(nbSize);
    int length = tmp.length();
    tmp = this->memory + QString::number(value);
    if (tmp.toInt() <= nbSize && this->memory.length() < length)
    {
        if (length == 2 && tmp != "00")
        {
            this->memory = tmp;
            setLabel(this->fMessage, this->sMessage + this->memory);
        }else if (length == 4){
            this->memory = tmp;
            setLabel(this->fMessage, this->sMessage + this->memory);
        }
    }
}

void MainWindow::on_pushButton_clicked() {
    buttonAction(1);
}

void MainWindow::on_pushButton_2_clicked() {
    buttonAction(2);
}

void MainWindow::on_pushButton_3_clicked() {
    buttonAction(3);
}
void MainWindow::on_pushButton_4_clicked() {
    buttonAction(4);
}

void MainWindow::on_pushButton_5_clicked() {
    buttonAction(5);
}

void MainWindow::on_pushButton_6_clicked() {
    buttonAction(6);
}

void MainWindow::on_pushButton_7_clicked() {
    buttonAction(7);
}

void MainWindow::on_pushButton_8_clicked() {
    buttonAction(8);
}

void MainWindow::on_pushButton_9_clicked() {
    buttonAction(9);
}

void MainWindow::on_pushButton_10_clicked() {
    buttonAction(0);
}

void MainWindow::on_pushButton_11_clicked()
{
    switch (this->screen)
    {
    case 0:
        if (this->memory.length() == 2)
        {
            if (this->memory == "*#")
            {
                this->screen = -1;
                this->fMessage = "Mode Administrateur";
                this->sMessage = "";
                setLabel(this->fMessage);

                this->memory = "";
                connect(t, SIGNAL(timeout()), this, SLOT(adminMode()));
                t->start(2000);
            }else{
                bdd.selectDrink(drink, this->memory.toInt() - 1);

                this->fMessage = "Vous avez sélectionnez " + drink->name + " : " + priceFormat(drink->price) + "€";
                this->sMessage = "1.Payer en vous identifiant 2.Payer en monnaie : ";
                setLabel(this->fMessage, this->sMessage);

                this->memory = "";
                this->screen = 1;
            }
        }else if (this->memory == "") {
            this->memory = "*";
            setLabel(this->fMessage, this->memory);
        }
        break;
    case 1:
        switch (this->memory.toInt())
        {
        case 1:
            this->sMessage = "Veuillez vous identifier : ";
            setLabel(this->fMessage, this->sMessage);

            this->screen = 2;
            break;
        case 2:
            this->sMessage = "Insérez la monnaie : ";
            setLabel(this->fMessage, this->sMessage);

            this->screen = 3;
            showMoney();
            break;
        }
        this->memory = "";
        break;
    case 2:
        if (this->memory.length() == 4)
        {
            if (bdd.selectUser(user, this->memory.toInt()) == true)
            {
                if ((drink->price - user->credit) > 0.1)
                {
                    if (user->credit < drink->price)
                    {
                        this->fMessage = "Solde insuffisant ! Manquant : " + priceFormat(drink->price - user->credit) + "€";
                        this->sMessage = "Insérez la monnaie : ";
                        setLabel(this->fMessage, this->sMessage);

                        this->memory = "";
                        this->screen = 4;
                        showMoney();
                    }else{
                        pay(0);
                    }
                }else{
                    pay(0);
                }
            }
        }
        break;
    case 3:
        if (this->memory.toFloat() >= drink->price && !m.close()) {
            pay(1);
        }
        break;
    case 4:
        if (this->memory.toFloat() >= drink->price && !m.close()) {
            pay(2);
        }
        break;
    case 5:
        switch (this->memory.toInt()) {
        case 1:
            this->fMessage = "Vous avez ajouter un utilisateur";
            this->sMessage = "Id de l'utilisateur : ";
            setLabel(this->fMessage, this->sMessage + idFormatU(bdd.addUser()));

            this->memory = "";
            connect(t, SIGNAL(timeout()), this, SLOT(adminMode()));
            t->start(4000);
            break;
        case 2:
            this->fMessage = "Sélectionner un utilisateur par son id";
            this->sMessage = "Id : ";
            setLabel(this->fMessage, this->sMessage);

            this->screen = 6;
            this->memory = "";
            break;
        case 3:
            u.setWindowIcon(QIcon(":pictures/pictures/icon.ico"));
            u.showNormal();
            u.ui->lId->setMaxLength(2);
            u.ui->lId->setValidator(intValidator);
            //u.ui->lPrice->setValidator(floatValidator);

            connect(u.ui->buttonBox,SIGNAL(accepted()),SLOT(updateDrink()));
            connect(u.ui->buttonBox,SIGNAL(rejected()),SLOT(updateDrink()));
            break;
        case 4:
            this->fMessage = "Sélectionnez une boisson";
            this->sMessage = "";
            setLabel(this->fMessage);
            this->screen = 7;
            this->memory = "";
            break;
        case 5:
            this->cancel = true;
            this->bdd.closeDatabase();
            this->close();
            break;
        default:
            break;
        }
        break;
    case 6:
        if (this->memory == "0000")
        {
            this->fMessage = "L'administrateur ne peut être supprimé !";
            this->sMessage = "";
            setLabel(this->fMessage);
            this->memory = "";
            connect(t, SIGNAL(timeout()), this, SLOT(adminMode()));
            t->start(2000);
        }else if (bdd.deleteUser(this->memory.toInt())){
            this->fMessage = "L'utilisateur " + this->memory + " à été supprimé";
            this->sMessage = "";
            setLabel(this->fMessage);
            this->memory = "";
            connect(t, SIGNAL(timeout()), this, SLOT(adminMode()));
            t->start(2000);
        }else{
            this->fMessage = "L'utilisateur " + this->memory + " n'existe pas";
            this->sMessage = "";
            setLabel(this->fMessage);
            this->memory = "";
        }
        break;
    case 7:
        bdd.selectDrink(drink, this->memory.toInt() - 1);
        this->fMessage = "Vous avez sélectionnez la boisson portant l'ID : " + idFormatD(drink->id);
        this->sMessage = "Nom : " + drink->name + " Prix : " + priceFormat(drink->price) + "€ Quantité vendue : " + QString::number(drink->qtySold);
        setLabel(this->fMessage, this->sMessage);
        this->memory = "";
        break;
    default:
        break;
    }
}

void MainWindow::on_pushButton_12_clicked()
{
    switch (this->screen) {
    case 0:
        if (this->memory == "*") {
            this->memory += "#";
        }else{
            this->memory = "";
        }
        setLabel(this->fMessage, this->memory);
        break;
    case 1:
        pushButton_12_action();
        break;
    case 2:
        pushButton_12_action();
        break;
    case 3:
        pushButton_12_action();
        break;
    case 5:
        pushButton_12_action();
        break;
    case 6:
        pushButton_12_action();
        break;
    case 7:
        adminMode();
        break;
    default:
        this->memory = "";
        this->screen = 0;

        if (!m.close())
        {
            m.cancel = true;
            m.close();
        }

        if (!u.close()){
            u.close();
        }
        welcomeMessage();
        break;
    }
}

void MainWindow::pushButton_12_action(){
    if (this->memory == "")
    {
        if (this->screen != 6)
        {
            if (this->screen == 3)
            {
                m.cancel = true;
                m.close();
            }
            t->stop();
            welcomeMessage();
        }else{
            adminMode();
        }
    }else{
        this->memory = "";
        if (this->screen != 5) {
            setLabel(this->fMessage, this->sMessage);
        }else{
            adminMode();
        }
    }
}

QString MainWindow::idFormatU(int id)
{
    QString tmp = QString::number(id);
    switch (tmp.length()) {
    case 1:
        tmp = "000" + tmp;
        break;
    case 2:
        tmp = "00" + tmp;
        break;
    case 3:
        tmp = "0" + tmp;
        break;
    default:
        break;
    }
    return tmp;
}

QString MainWindow::idFormatD(int id)
{
    QString tmp = QString::number(id + 1);
    if (tmp.length() == 1) {
        tmp = "0" + tmp;
    }
    return tmp;
}

QString MainWindow::priceFormat(float price)
{
    QString tmp = QString::number(price);
    switch (tmp.length())
    {
    case 1:
        tmp += ".00";
        break;
    case 3:
        tmp += "0";
        break;
    default:
        break;
    }
    return tmp;
}
