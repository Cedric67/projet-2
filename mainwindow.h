#ifndef MAINWINDOW_H
#define MAINWINDOW_H

/*!
 * \file mainwindow.h
 * \brief Description de la classe MainWindow.
 * \author SCHULTZ Cédric
 * \version 1.0
 * \date 11/04/2018
 * \details Le MainWindow est la fenêtre principale à partir de laquelle toutes les actions sont effectuée.
 */

#include <QMainWindow>
#include <QTimer>
#include <QThread>
#include "database.h"
#include "money.h"
#include "ui_money.h"
#include "updrinks.h"
#include "ui_updrinks.h"
#include "drinks.h"
#include "users.h"

/*!
 * \namespace Ui
 * \brief Déclaration d'utilisation de la \b Forms mainwindow.ui.
 */
namespace Ui {

/*!
 * \class MainWindow
 * \brief Intégration de la classe MainWindow dans le \b namespace.
 */
class MainWindow;
}

/*!
 * \class MainWindow
 * \brief La classe MainWindow hérite de l'objet QMainWindow, elle contient tous les objets, fonctions, attribues nécessaires à la gestion du programme.
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    /*!
     * \fn explicit MainWindow
     * \param *parent
     */
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    /*!
     * \fn void on_pushButton_clicked()
     * \brief Bouton portant le numéro \b 1
     * Exécute la fonction buttonAction
     */
    void on_pushButton_clicked();

    /*!
     * \fn void on_pushButton_2_clicked()
     * \brief Bouton portant le numéro \b 2
     * Exécute la fonction buttonAction
     */
    void on_pushButton_2_clicked();

    /*!
     * \fn void on_pushButton_3_clicked()
     * \brief Bouton portant le numéro 3
     * Exécute la fonction buttonAction
     */
    void on_pushButton_3_clicked();

    /*!
     * \fn void on_pushButton_4_clicked()
     * \brief Bouton portant le numéro \b 4
     * Exécute la fonction buttonAction
     */
    void on_pushButton_4_clicked();

    /*!
     * \fn void on_pushButton_5_clicked()
     * \brief Bouton portant le numéro 5
     * Exécute la fonction buttonAction
     */
    void on_pushButton_5_clicked();

    /*!
     * \fn void on_pushButton_6_clicked()
     * \brief Bouton portant le numéro \b 6
     * Exécute la fonction buttonAction
     */
    void on_pushButton_6_clicked();

    /*!
     * \fn void on_pushButton_7_clicked()
     * \brief Bouton portant le numéro \b 7
     * Exécute la fonction buttonAction
     */
    void on_pushButton_7_clicked();

    /*!
     * \fn void on_pushButton_8_clicked()
     * \brief Bouton portant le numéro 8
     * Exécute la fonction buttonAction
     */
    void on_pushButton_8_clicked();

    /*!
     * \fn void on_pushButton_9_clicked()
     * \brief Bouton portant le numéro \b 9
     * Exécute la fonction buttonAction
     */
    void on_pushButton_9_clicked();

    /*!
     * \fn void on_pushButton_10_clicked()
     * \brief Bouton portant le numéro \b 0
     * Exécute la fonction buttonAction
     */
    void on_pushButton_10_clicked();

    /*!
     * \fn void on_pushButton_11_clicked()
     * \brief Bouton multifonctionnel correspondant au bouton \b *.
     * \details Pour la multifonctionnalité il y a un \b switch sur la variable \b screen.
     * Lorsque la variable \b memory répond aux conditions du \b case en question, le bouton valide la saisie et modifie la variable \b screen pour aller au \b case suivant.
     * Il modifie ainsi l'affichage de l'écran et permet à l'utilisateur de naviger dans le programme.
     * Particularité : A l'écran principale, s'il n'y a pas de saisie, le bouton affiche \b *, si la saisie est *#, le programme passe en Mode administrateur.
     */
    void on_pushButton_11_clicked();

    /*!
     * \fn void on_pushButton_12_clicked()
     * \brief Bouton multifonctionnel correspondant au bouton #.
     * \details Le bouton permet d'annuler la saisie et de revenir en arrière dans le menu.
     * Particularité : A l'écran principale, si la saisie est *, le bouton affiche \b #.
     */
    void on_pushButton_12_clicked();

    /*!
     * \fn void welcomeMessage()
     * \brief Affiche l'écran principale de sélection de boisson.
     */
    void welcomeMessage();

    /*!
     * \fn void adminMode()
     * \brief Affiche et lance l'écran d'administration.
     * \details Lance la fonction adminMessage qui compose une première partie de la boucle de message.
     * Cette boucle prend fin lorsque l'utilisateur aura effectué une saisie.
     */
    void adminMode();

    /*!
     * \fn void adminMessage
     * \brief Affiche la première partie du menu d'administration.
     * \details Lance la fonction adminMessage2.
     */
    void adminMessage();

    /*!
     * \fn void adminMessage2
     * \brief Affiche la deuxième partie du menu d'administration.
     * \details Lance la fonction adminMessage3.
     */
    void adminMessage2();

    /*!
     * \fn void adminMessage3
     * \brief Affiche la dernière partie du menu d'administration.
     * \details Lance la fonction adminMessage.
     */
    void adminMessage3();

    /*!
     * \fn void addMoney
     * \brief Affiche la monnaie injectée via la fenêtre money à l'écran.
     */
    void addMoney();

    /*!
     * \fn void updateDrink
     * \brief Affiche à l'écran les modifications apporté à une boisson.
     * \details L'administrateur à la possibilité de modifier une boisson en choisissant l'option \b 3 du menu administrateur.
     * Le cas échéant, les modifications sont affichés à l'écran.
     */
    void updateDrink();

private:
    /*!
     * \brief *ui
     * \details Pointeur ver l'IHM mainwindow.ui.
     */
    Ui::MainWindow *ui;

    /*!
     * \brief bdd
     * \details L'objet \b bdd hérite de la classe \b database.
     */
    database bdd;

    /*!
     * \brief *drink
     * \details Le pointeur hérite de la classe \b drinks.
     */
    drinks* drink = new drinks;

    /*!
     * \brief *user
     * \details Le pointeur hérite de la classe \b users.
     */
    users* user = new users;

    /*!
     * \brief u
     * \details L'objet \b u hérite de la classe \b UpDrinks.
     */
    UpDrinks u;

    /*!
     * \brief m
     * \details L'objet \b u hérite de la classe \b Money.
     */
    Money m;

    /*!
     * \brief mui
     * \details L'objet \b u hérite de la classe \b Ui_Money.
     */
    Ui_Money mui;

    /*!
     * \fn void Initialize
     * \brief Initialise le programme
     * \details Initialisation des variables, de la base de données, de l'écran et des images.
     */
    void Initialize();

    /*!
     * \fn void selection
     * \param value
     * \param nbSize
     * \brief Fonction de validation de saisie.
     * \details Fonction qui vérifie si la variable \b memory est inférieur ou égale à \a value et que la longueur de la chaîne inférieur ou égale à \a nbSize.
     */
    void selection(int value, int nbSize);

    /*!
     * \fn void setLabel
     * \param  Line
     * \overload void setLabel(QString fLine, QString sLine)
     * \brief Fonction qui exécute la modification (setter) de l'affichage de l'écran.
     */
    void setLabel(QString Line);

    /*!
     * \fn void setLabel
     * \param  fLine
     * \param sLine
     * \overload void setLabel(QString Line)
     * \brief Fonction surcharge de la précédente.
     */
    void setLabel(QString fLine, QString sLine);

    /*!
     * \fn void buttonAction
     * \param buttonId
     * \brief Fonction qui gère les actions des buttons en fonction de la valeur de la variable \b screen.
     * \details Lance la fonction \b selection lorsque la variable \b screen est égale à 0, 2, 6 et 7.
     * Pour les cas 1 et 5, la fonction vérifie si la saisie répond aux conditions.
     * La fonction échappe les autres cas.
     */
    void buttonAction(int buttonId);

    /*!
     * \fn void pushButton_12_action
     * \brief La fonction effectue certaines actions du bouton \b pushButton_12.
     * \details La fonction peut fermer la fenêtre \b Money, lancer la fonction welcomeMessage, lancer la fonction adminMode et effacer la valeur de la variable \b memory.
     */
    void pushButton_12_action();

    /*!
     * \fn void pay
     * \param userType
     * \brief Effectue le paiement/transaction de l'utilisteur.
     * \details La fonction ferme la fenêtre \b Money, augmente de 1 la quantité vendue de la boisson et affiche un message à l'utilisateur avant de lancer la fonction \b welcomeMessage.
     * \a userType représente 3 approches. Soit l'utilisateur paie en monnaie, soit l'utilisateur paie avec son compte, soit l'utilisateur paie avec son compte mais avec un solde insuffisant.
     */
    void pay(int userType);

    /*!
     * \fn void showMoney
     * \brief Ouvre la fenêtre Money et établie un \b connect avec les boutons et le \b slot addMoney.
     */
    void showMoney();

    /*!
     * \fn void closeEvent
     * \param *e
     * \brief Fonction surcharge de la méthode closeEvent.
     * \details Contrôle la fermeture de la fenêtre ainsi que de l'application et empêche la fermeture par la croix.
     */
    void closeEvent(QCloseEvent *e);

    /*!
     * \brief *intValidator est un pointeur de validation (QValidator) de integer. Il empêche toutes autres saisies dans le \b QLineEdit \b lId.
     */
    QValidator *intValidator = new QIntValidator(this);

    //QValidator *floatValidator = new QDoubleValidator(this); //Pointeur sensé valider les float mais bloque les "."

    /*!
     * \brief *t est un pointeur de timer utiliser pour la plupart des \b connect.
     */
    QTimer *t = new QTimer(this);

    /*!
     * \fn QString idFormatU
     * \param id
     * \return QString tmp
     * \details Fonction qui met en forme l'\b id de l'\b utilisateur.
     */
    QString idFormatU(int id);

    /*!
     * \fn QString idFormatD
     * \param id
     * \return QString tmp
     * \details Fonction qui met en forme l'\b id de la \b boisson.
     */
    QString idFormatD(int id);

    /*!
     * \fn QString priceFormat
     * \param price
     * \return QString tmp
     * \details Fonction qui met en forme le \b prix de la boisson.
     */
    QString priceFormat(float price);

    float getFloat(float value);

    /*!
     * \brief String qui enregistre toutes les saisies de l'utilisateur.
     */
    QString memory = "";

    /*!
     * \brief Variable String qui contient le message afficher en première ligne de l'écran (QLabel).
     */
    QString fMessage = "";

    /*!
     * \brief Variable String qui contient le message afficher en deuxième ligne de l'écran (QLabel).
     */
    QString sMessage = "";

    /*!
     * \brief Integer initialisé à -1, il permet d'afficher les différents messages à l'écran et empêche toute saisies lorsqu'il est à -1.
     */
    int screen = -1;

    /*!
     * \brief Booléen qui autorise ou non la fermeture de la fenêtre et de l'application.
     */
    bool cancel = false;
};

#endif // MAINWINDOW_H
