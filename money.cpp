#include "money.h"
#include "ui_money.h"

Money::Money(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Money)
{
    ui->setupUi(this);
}

Money::~Money()
{
    delete ui;
}

void Money::closeEvent(QCloseEvent *e)
{
    if (this->cancel == false)
    {
        e->ignore();
    }else{
        e->accept();
        this->amount = 0;
    }
    this->cancel = false;
}

void Money::sum(float value)
{
    this->amount += value;
}

void Money::on_pushButton_clicked()
{
    sum(0.10);
}

void Money::on_pushButton_2_clicked()
{
    sum(0.20);
}
void Money::on_pushButton_3_clicked()
{
    sum(0.50);
}

void Money::on_pushButton_4_clicked()
{
    sum(1);
}

void Money::on_pushButton_5_clicked()
{
    sum(2);
}
