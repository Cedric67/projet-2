#ifndef DATABASE_H
#define DATABASE_H

/*!
 * \file database.h
 * \brief Description de la classe database.
 * \details La classe \b database permet la création et la manipulation de base de données.
 */

#include <QtSql/QtSql>
#include <QMessageBox>
#include "users.h"
#include "drinks.h"

/*!
 * \class database
 * \brief La classe database contient tous les objets, fonctions et variables nécessaires au projet pour la création et la manipulation de base de données.
 */
class database
{
public:
    /*!
     * \brief Constructeur.
     */
    database();

    /*!
     * \brief Objet permettant la création, la connection et l'utilisation de base de données SQLite.
     */
    QSqlDatabase db;

    /*!
     * \fn void addDatabase
     * \param name
     * \details Fonction qui créé une base de données SQLite.
     */
    void addDatabase(QString name);

    /*!
     * \fn bool openDatabase
     * \return bool
     * \brief Ouvre la base de données et renvoie un booléen pour confirmer l'ouverture ou non.
     */
    bool openDatabase();

    /*!
     * \fn void closeDatabase
     * \brief Ferme la base de données.
     */
    void closeDatabase();

    /*!
     * \fn void initializeDatabase
     * \param name
     * \brief Fonction qui lance les sous-fonctions créant et remplissant les deux tables à la création de la base de données.
     */
    void initializeDatabase(QString name);

    /*!
     * \fn bool executeQuery
     * \param request
     * \return bool
     * \brief Fonction qui qui execute la requête et renvoie un booléen de confirmation ou d'échec.
     */
    bool executeQuery(QString request);

    /*!
     * \fn int addUser
     * \return int
     * \brief Fonction qui permet d'ajouter un utilisateur dans la base de données et revoyant l'\b id du nouvel utilisateur.
     */
    int addUser();

    /*!
     * \fn bool deleteUser
     * \param id
     * \return bool
     * \brief Fonction supprimant un utilisateur de la base de données.
     */
    bool deleteUser(int id);

    /*!
     * \fn bool updateCredit
     * \param *user
     * \return bool
     * \brief Fonction mettant à jour le crédit de l'utilisateur et renvoyant un booléen de confirmation ou d'échec.
     */
    bool updateCredit(users* user);

    /*!
     * \fn bool selectUser
     * \param *user
     * \param id
     * \return bool
     * \brief Fonction vérifiant si l'utilisateur existe et le cas échéant réccupérer ses informations. Renvoie un booléen de confirmation ou d'échec.
     */
    bool selectUser(users* user, int id);

    /*!
     * \fn bool updateDrink
     * \param *drink
     * \return bool
     * \brief Fonction mettant à jour la boisson et renvoyant un booléen de confirmation ou d'échec.
     */
    bool updateDrink(drinks* drink);

    /*!
     * \fn void selectDrink
     * \param *drink
     * \param id
     * \brief Fonction réccupérant les informations de la boisson.
     */
    void selectDrink(drinks* drink, int id);

private:
    /*!
     * \brief L'objet \b d hérite de la classe \b drinks.
     */
    drinks d;

    /*!
     * \brief L'objet \b u hérite de la classe \b users.
     */
    users u;

    /*!
     * \fn void addDrinks
     * \brief Fonction qui ajoute les boissons par défauts dans la table \b drinks à la création de la base de données.
     */
    void addDrinks();

    /*!
     * \fn int countRows
     * \param name
     * \return int
     * \brief Fonction contant le nombre de ligne dans la table et renvoyant ce nombre.
     */
    int countRows(QString name);

    /*!
     * \brief Variable String réccupérant le nom de la table.
     */
    QString name;
};

#endif // DATABASE_H
