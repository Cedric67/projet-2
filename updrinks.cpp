#include "updrinks.h"
#include "ui_updrinks.h"

UpDrinks::UpDrinks(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::UpDrinks)
{
    ui->setupUi(this);
}

UpDrinks::~UpDrinks()
{
    delete ui;
}

void UpDrinks::on_buttonBox_accepted()
{
    change = false;
    if (ui->lId->text().length() > 1 && ui->lName->text() != "" && ui->lPrice->text() != "")
    {
        this->id = ui->lId->text().toInt() - 1;
        drink->id = ui->lId->text().toInt() - 1;
        drink->name = ui->lName->text();
        drink->price = ui->lPrice->text().toFloat();
        drink->qtySold = 0;
        if (bdd.updateDrink(drink))
        {
            change = true;
            this->close();
        }else{
            QMessageBox::warning(this,"Update error","L'Id n'est pas valide !");
        }
        ui->lId->setText("");
        ui->lName->setText("");
        ui->lPrice->setText("");
    }
}

void UpDrinks::on_buttonBox_rejected()
{
    change = false;
    ui->lId->setText("");
    ui->lName->setText("");
    ui->lPrice->setText("");
    this->close();
}
