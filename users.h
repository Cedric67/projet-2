#ifndef USERS_H
#define USERS_H

/*!
 * \file users.h
 * \brief Description de la classe users.
 * \details La classe \b users contient les attribues des utilisateurs.
 */

/*!
 * \class users
 * \brief La classe users contient 2 attribues nécessaires pour les utilisateurs.
 */
class users
{
public:
    /*!
     * \brief Constructeur.
     */
    users();

    /*!
     * \brief Integer prenant l'\b id de l'utilisateur.
     */
    int id;

    /*!
     * \brief Float prenant le \b crédit de l'utilisateur.
     */
    float credit;
};

#endif // USERS_H
